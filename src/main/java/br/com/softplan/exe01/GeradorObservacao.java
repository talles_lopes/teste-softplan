package br.com.softplan.exe01;


import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class GeradorObservacao { 

	 
	static final String UMANOTA = "Fatura da nota fiscal de simples remessa: ";
	static final String NOTAS = "Fatura da notas fiscais de simples remessa: ";
	static final String CUJOVALOR = "cujo valor é ";
	static final String MOEDA = "R$:";
	static final String SEPARADOR = ",";
	static final String FRASEFINAL = "e";
	String texto;
		
	/**
	 * 	//Gera observações, com texto pre-definido, incluindo os números, das notas fiscais, recebidos no parâmetro
	 * @param lista
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public String geraObservacao(List lista) 
	{
		texto = "";
		if (!lista.isEmpty()) 
		{
			return retornaCodigos(lista) + ".";
		}		
		return "";		
	}

	
	/**
	 * //Cria observação
	 * @param lista
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private String retornaCodigos(List lista) {
		if (lista.size() >= 2) {
			texto = NOTAS;
		} else {
			texto = UMANOTA;
		}
		
	
		StringBuilder cod = new StringBuilder();
		for (Iterator<Integer> iterator = lista.iterator(); iterator.hasNext();) {
			Integer c = iterator.next();
			String s = "";
			if( cod.toString() == null || cod.toString().length() <= 0 )
				s =  "";
				else if( iterator.hasNext() )
					s =  SEPARADOR;
				else
					s =  FRASEFINAL;
			
			System.out.println(cod.append(s + CUJOVALOR + MOEDA + c));
		}
		
		return texto + cod;
	}
	
	public static void main(String[] args) {
		GeradorObservacao geradorObservacao = new GeradorObservacao();
		List<Integer> numbers = Arrays.asList(1,2,3,4,5);
		geradorObservacao.geraObservacao(numbers);
	}
}
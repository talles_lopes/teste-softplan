package br.com.softplan.exe02;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class LerArquivoJson {

	public static void main(String[] args) {
		LerArquivoJson.getArquivo();
	}

	@SuppressWarnings("unchecked")
	public static JSONArray getArquivo() {
		JSONParser jsonParser = new JSONParser();
		JSONArray arquivo = null;

		try (FileReader reader = new FileReader("src/main/resources/dados-entrada-servicos-composicoes.json")) {

			Object obj = jsonParser.parse(reader);
			arquivo = (JSONArray) obj;
			System.out.println(arquivo);

			arquivo.forEach(item -> converterJsonCampos((JSONObject) item));

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return arquivo;

	}

	/**
	 * Parse campos do arquivo JSON
	 * 
	 * @param object
	 */
	private static void converterJsonCampos(JSONObject object) {

		Long codigoComposicao = (Long) object.get("codigoComposicao");
		System.out.println(codigoComposicao);

		String descricaoComposicao = (String) object.get("descricaoComposicao");
		System.out.println(descricaoComposicao);

		String unidadeComposicao = (String) object.get("unidadeComposicao");
		System.out.println(unidadeComposicao);

		String tipoItem = (String) object.get("tipoItem");
		System.out.println(tipoItem);

		Long codigoItem = (Long) object.get("codigoItem");
		System.out.println(codigoItem);

		String descricaoItemComposicao = (String) object.get("descricaoItemComposicao");
		System.out.println(descricaoItemComposicao);

		String unidadeItem = (String) object.get("unidadeItem");
		System.out.println(unidadeItem);

		String quantidadeComposicao = (String) object.get("quantidadeComposicao");
		System.out.println(quantidadeComposicao);

		String valorUnitario = (String) object.get("valorUnitario");
		System.out.println(valorUnitario);

	}

}